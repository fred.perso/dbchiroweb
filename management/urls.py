from django.urls import path

from .regulatory.views import CatchAuthCreate, CatchAuthDelete, CatchAuthList, CatchAuthUpdate
from .study.api import StudyDatasApi, StudyListApi
from .study.views import StudyCreate, StudyDelete, StudyDetail, StudyList, StudyUpdate
from .transmitter.api import TransmitterSearchApi
from .transmitter.views import (
    TransmitterCreate,
    TransmitterDelete,
    TransmitterList,
    TransmitterUpdate,
)

app_name = "management"

urlpatterns = [
    # Study relative URLS
    path("study/add", StudyCreate.as_view(), name="study_create"),
    path("study/<int:pk>/edit", StudyUpdate.as_view(), name="study_update"),
    path("study/<int:pk>/delete", StudyDelete.as_view(), name="study_delete"),
    path("study/list", StudyList.as_view(), name="study_list"),
    path("api/v1/study/list", StudyListApi.as_view(), name="api_study_list"),
    path("study/list/<int:page>", StudyList.as_view(), name="url_study_list"),
    path("study/<int:pk>/detail", StudyDetail.as_view(), name="url_study_detail"),
    # path(
    #     "api/v1/study/<int:pk>/observations",
    #     StudySightingsApi.as_view(),
    #     name="api_study_sightings",
    # ),
    path("api/v1/study/<int:pk>/datas", StudyDatasApi.as_view(), name="api_study_data"),
    # Transmitter relative URLS
    path("transmitter/add", TransmitterCreate.as_view(), name="transmitter_create"),
    path(
        "transmitter/<int:pk>/update",
        TransmitterUpdate.as_view(),
        name="transmitter_update",
    ),
    path(
        "transmitter/<int:pk>/delete",
        TransmitterDelete.as_view(),
        name="transmitter_delete",
    ),
    path("transmitter/list", TransmitterList.as_view(), name="transmitter_list"),
    path(
        "transmitter/list/<int:page>",
        TransmitterList.as_view(),
        name="url_transmitter_list",
    ),
    path(
        "api/v1/transmitter/search",
        TransmitterSearchApi.as_view(),
        name="transmitter_search_api",
    ),
    # CatchAuth relative URLS
    path("catchauth/add", CatchAuthCreate.as_view(), name="catchauth_create"),
    path("catchauth/<int:pk>/update", CatchAuthUpdate.as_view(), name="catchauth_update"),
    path("catchauth/<int:pk>/delete", CatchAuthDelete.as_view(), name="catchauth_delete"),
    path("catchauth/list", CatchAuthList.as_view(), name="catchauth_list"),
    path("catchauth/list/<int:page>", CatchAuthList.as_view(), name="url_catchauth_list"),
]
