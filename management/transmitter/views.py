from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, UpdateView
from django_tables2.views import SingleTableView

from core import js

from ..forms import TransmitterForm
from ..mixins import ManagementAuthMixin
from ..models import Transmitter
from ..tables import TransmitterTable


class TransmitterCreate(LoginRequiredMixin, CreateView):
    """Create view for the Transmitter model."""

    model = Transmitter
    form_class = TransmitterForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-podcast"
        context["title"] = _("Ajout d'un émetteur")
        context["js"] = js.DateInput
        return context


class TransmitterUpdate(LoginRequiredMixin, UpdateView):
    """Update view for the Transmitter model."""

    model = Transmitter
    form_class = TransmitterForm
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-podcast"
        context["title"] = _("Modification d'un émetteur")
        context["js"] = js.DateInput
        return context


class TransmitterDelete(ManagementAuthMixin, DeleteView):
    """Delete view for the Transmitter model."""

    model = Transmitter
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("management:transmitter_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-trash"
        context["title"] = _("Suppression d'un émetteur")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer l'émetteur")
        return context


class TransmitterList(LoginRequiredMixin, SingleTableView):
    table_class = TransmitterTable
    model = Transmitter
    template_name = "transmitter_list.html"
    table_pagination = {"per_page": 25}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-podcast"
        context["title"] = _("Liste des émetteurs")
        context[
            "js"
        ] = """
        """
        return context
