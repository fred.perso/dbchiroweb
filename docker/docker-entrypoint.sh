#!/bin/bash

echo ""
echo "dbChiro init_____________________________________________"
echo ""
echo "              *         *      *         *"
echo "          ***          **********          ***"
echo "       *****           **********           *****"
echo "     *******           **********           *******"
echo "   **********         ************         **********"
echo "  ****************************************************"
echo " ******************************************************"
echo "********************************************************"
echo "********************************************************"
echo "********************************************************"
echo " ******************************************************"
echo "  ********      ************************      ********"
echo "   *******       *     *********      *       *******"
echo "     ******             *******              ******"
echo "       *****             *****              *****"
echo "          ***             ***              ***"
echo "            **             *              **"
echo ""


export POSTGRES_HOST=${POSTGRES_HOST:-db}
export POSTGRES_DB=${POSTGRES_DB:-dbchiro}
export POSTGRES_PORT=${POSTGRES_PORT:-5432}

printenv

shopt -s dotglob nullglob


until pg_isready -h $POSTGRES_HOST -p $POSTGRES_PORT; do
    echo "Awaiting Database container on ${POSTGRES_HOST}:${POSTGRES_PORT}"
    sleep 1
done
sleep 2

PGPASSWORD=$POSTGRES_PASSWORD psql -U $POSTGRES_USER -d $POSTGRES_DB -h $POSTGRES_HOST -p $POSTGRES_PORT -c 'create extension postgis_raster;'

cd /app

echo "************ Init database ****************"

python3 -m manage migrate
echo "dbChiro Database ready"

python3 -m manage collectstatic --noinput
echo "Static files collected"

if [ ! -f /app/media/logo_site.png ]; then
    echo "Create default site logo image"
    mkdir -p /app/media
    cp /app/static/img/logo_site.png.sample /app/media/logo_site.png
else
    echo "Site logo already exists"
fi
echo "************** Create dbChiro SU ******************"

script="
from accounts.models import Profile;

username = '$DBCHIRO_SU_USERNAME';
password = '$DBCHIRO_SU_PWD';
email = '$DBCHIRO_SU_EMAIL';

if Profile.objects.filter(is_superuser=True).count()==0:
    superuser=Profile.objects.create_user(username, email, password);
    superuser.is_superuser=True;
    superuser.is_staff=True;
    superuser.save();
    print('Superuser',username,'created.');
else:
    print('One or more Superuser already exists, creation skipped.')
"
printf "$script" | python3 manage.py shell

echo "************** Populate with initial data ***************"

echo "load dicts data"
python3 -m manage loaddata dicts/fixtures/dicts.xml
python3 -m manage loaddata sinp_dict_data_v1.0.json
python3 -m manage loaddata nomenclatures_dbchiro.json
python3 -m manage loaddata inpn_nomenclatures_organisms.json
echo "dicts data loaded"


export countorganisms=$(printf "from sinp_organisms.models import Organism; print(Organism.objects.count())" | python3 manage.py shell)
if [ $countorganisms -eq 0 ]; then
    echo "load organisms, this may take a while..."
    python3 -m manage loaddata inpn_organisms.json
    echo "organisms loaded"
else
    echo "organisms already exists, if you wan't to update, please execute 'python3 -m manage loaddata inpn_organisms.json'"
fi

export countterritory=$(printf "from geodata.models import Areas; print(Areas.objects.count())" | python3 manage.py shell)

if [ $countterritory -eq 0 ]; then
    echo "load geo datas, this may take a while..."
    python3 -m manage loaddata dep.xml.gz
    python3 -m manage loaddata mun.xml.gz
    echo "geo datas loaded"
else
    echo "geo datas already exists"
fi

echo "************ Start Gunicorn ***************"
cd /app
if [ $DEBUG = true ]
then
    echo "Starting dev mode"
    python -m manage runserver 0.0.0.0:8000
else
    echo "Starting prod mode"
    gunicorn -b 0.0.0.0:8000 -t ${GUNICORN_TIMEOUT:-180} -w ${GUNICORN_WORKERS:-1} dbchiro.wsgi
fi
