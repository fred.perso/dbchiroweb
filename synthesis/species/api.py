import logging

from django.db.models import Aggregate, Count, Max, Q
from django.db.models.fields import BooleanField
from django.db.models.functions import ExtractYear
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_gis.filters import InBBoxFilter

from dicts.models import AreaType
from geodata.models import Areas

from .serializers import DistributionSerialiazer

logger = logging.getLogger(__name__)

qs = (
    Areas.objects.select_related("area_type")
    # .filter(places__isnull=False)
    # .exclude(places__sessions__observations__is_doubtful=True)
    # .prefetch_related("places")
    # .prefetch_related("places__sessions")
    # .prefetch_related("places__sessions__observations")
    # .prefetch_related("places__sessions__observations__codesp")
)


class BoolOr(Aggregate):
    function = "bool_or"
    output_field = BooleanField()


class SpecieDistribution(ListAPIView):
    queryset = qs
    pagination_class = None
    filter_backends = (DjangoFilterBackend, InBBoxFilter)
    filter_fields = ["area_type"]
    serializer_class = DistributionSerialiazer
    bbox_filter_include_overlapping = True
    permission_classes = [
        IsAuthenticated,
    ]
    # filter_fields = ["area_type", "places__sessions__sightings__codesp"]

    def get_queryset(self):
        qs = super().get_queryset()
        codesp = self.request.query_params.get("codesp", None)
        type = self.request.query_params.get("type", None)
        period = self.request.query_params.get("period", None)

        filters = {}
        if period:
            filters["places__sessions__observations__period"] = period
        if codesp:
            filters["places__sessions__observations__codesp"] = codesp
        if type:
            filters["area_type"] = AreaType.objects.get(pk=type)

        qs = qs.filter(**filters)

        qs = (
            qs.annotate(
                count_observations=Count("places__sessions__observations", distinct=True),
                count_taxa=Count(
                    "places__sessions__observations__codesp",
                    filter=Q(places__sessions__observations__codesp__sp_true=True),
                    distinct=True,
                ),
                count_sessions=Count("places__sessions", distinct=True),
                count_places=Count("places", distinct=True),
                last_data=Max(ExtractYear("places__sessions__date_start")),
                breed_colo=BoolOr("places__sessions__observations__breed_colo"),
                has_gite=BoolOr("places__is_gite"),
            )
            .distinct()
            .all()
        )
        return qs
