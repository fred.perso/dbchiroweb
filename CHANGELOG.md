# CHANGELOG

## 1.5.4-dev - 2024-xx-xx

- Change organism members management (can now select multiple member level).
- Studies are now optionnaly required on sessions.
- New "add study link" on session form
- Standardize verbose names for confidential fields
- Update docs
- Add place actors management, place actors can now be declared on places (managers, owners, etc.)

## 1.5.3 - 2024-11-06

- Add Antille species (disabled by default) (fix #)
- Configure Natural primary key for dicts
- Improve admin UI for species groupings
- Rename 'Site sensible' to 'Site confidentiel', 'Observateur principal' to 'Responsable de session'
- Reorder count details form inputs
- Fix missing measurement for "D3"
- Reorder biometric fields

## 1.5.2 - 2024-10-14

- Fix save error on new place due to new IGN API Url (fix #133)

## 1.5.1 - 2024-10-11

- Fix save issue on observations while sights extra_data is null (fix #132)

## 1.5.0 - 2024-10-08

- Temporary patch to manage outdated nomenclatures, waiting for a full migration to `dj_sinp_nomenclatures`
- New fields on count details:
  - biologic status (dead or alive!)
  - behaviour
  - observers (text field, saved within json `extra_data` field)
- Fix disappeared dicts management on backoffice
- Add custom actions on dicts models to enable/disable items on models with enable booleand field
- New external organisms
- Fix performance issues
- Better documentation for docker deployement
- Many other fixes...
