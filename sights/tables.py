import django_tables2 as tables
from django.utils.translation import gettext_lazy as _
from django_tables2.utils import A

from sights.models import (
    Bridge,
    Build,
    Cave,
    CountDetail,
    Device,
    Metaplace,
    Place,
    PlaceManagement,
    Session,
    Sighting,
    Tree,
    TreeGite,
)

table_class = {"class": "table table-striped table-condensed"}

COMMENT = """
        {% if record.comment %}
        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                data-target=".bs-metaplace-comment-modal-md-{{ record.pk }}">
            <i class="fa fa-comment" aria-hidden="true"></i>
        </button>
        <div class="modal fade bs-metaplace-comment-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="comment">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-comment"></i>Commentaire</h4>
                    </div>
                    <div class="modal-body">
                        <p>{{ record.comment|safe }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {% endif %}
    """


class MetaplaceTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-xs">
                <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Action <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-toggle="modal"
                           data-target=".bs-metaplace-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                            class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                    <li><a href="{% url 'sights:metaplace_detail' record.pk %}" title="Fiche détaillée"><i class="fa fa-fw fa-info-circle"></i> Fiche
                        détaillée</a></li>
                    <li><a href="{% url 'sights:metaplace_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                    </li>
                    <li style="color:red;"><a href="{% url 'sights:metaplace_delete' record.pk %}" title="Supprimer"><i
                            class="fa fa-fw fa-trash"></i> Supprimer</a></li>
                </ul>
            </div>
            <div class="modal fade bs-metaplace-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="history">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                        </div>
                        <div class="modal-body">
                            <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                {% if record.timestamp_update %}
                                    et mise à jour le {{ record.timestamp_update }}
                                {% endif %}
                                par {{ record.updated_by }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            """

    MAPLOCATION = """
        {% if record.geom %}
            <button class="btn btn-primary btn-xs map-navigation" data-zoom="15" data-position="{{ record.get_centroid }}"><i class="fas fa-map-marker-alt"></i></button>
        {% endif %}
        """

    # id = tables.Column(accessor='id_place', verbose_name='id')
    places = tables.Column(
        accessor="place_set.all.count", verbose_name=_("Localités"), orderable=False
    )
    name = tables.LinkColumn("sights:metaplace_detail", args=[A("id_metaplace")], accessor="name")
    maplocation = tables.TemplateColumn(MAPLOCATION, verbose_name="Carte", orderable=False)
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    type = tables.Column(accessor="type.descr", verbose_name="Type de gîte")
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Metaplace
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "maplocation",
            "name",
            "type",
            "created_by",
            "places",
            "comment",
        )


class PlaceTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-xs">
                <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Action <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-toggle="modal"
                           data-target=".bs-place-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                            class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                    <li><a href="{% url 'sights:place_detail' record.pk %}" title="Fiche détaillée"><i class="fa fa-fw fa-info-circle"></i> Fiche
                        détaillée</a></li>
                    <li><a href="{% url 'sights:place_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                    </li>
                    <li><a href="{% url 'sights:session_create' record.pk %}" Title="Ajouter une session"><i
                            class="far fa-calendar-alt"></i> Ajouter une session</a></li>
                    <li role="separator" class="divider"></li>
                    <li style="color:red;"><a href="{% url 'sights:place_delete' record.pk %}" title="Supprimer"><i
                            class="fa fa-fw fa-trash"></i> Supprimer</a></li>
                </ul>
            </div>
            <div class="modal fade bs-place-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="history">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                        </div>
                        <div class="modal-body">
                            <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                {% if record.timestamp_update %}
                                    et mise à jour le {{ record.timestamp_update }}
                                {% endif %}
                                par {{ record.updated_by }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            """
    MAPLOCATION = """
    {% if record.geom %}
        <button class="btn btn-primary btn-xs map-navigation" data-zoom="15" data-position="{{ record.geom.y}}-{{ record.geom.x}}"><i class="fas fa-map-marker-alt"></i></button>
    {% endif %}
    """
    # id = tables.Column(accessor='id_place', verbose_name='id')
    nbsession = tables.Column(
        accessor="sessions.all.count", verbose_name="Sessions", orderable=False
    )
    name = tables.LinkColumn("sights:place_detail", args=[A("id_place")], accessor="name")
    maplocation = tables.TemplateColumn(MAPLOCATION, verbose_name="Carte", orderable=False)
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    type = tables.Column(accessor="type.descr", verbose_name="Type de gîte")
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Place
        template = "table_bootstrap.html"
        attrs = {"class": "table table-striped table-condensed", "id": "place_results"}
        fields = (
            "actions",
            "maplocation",
            "name",
            "is_hidden",
            "type",
            "altitude",
            "created_by",
            "nbsession",
            "comment",
        )


class PlaceManagementTable(tables.Table):
    ACTIONS = """
        <div class="btn-group btn-group-primary btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-mgmt-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:management_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li><a href="{% url 'sights:management_create' record.pk %}" Title="Ajouter une observation"><i
                        class="fi-eye"></i> Ajouter une action</a></li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:management_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-mgmt-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = PlaceManagement
        template = "table_bootstrap.html"
        attrs = table_class
        fields = ("actions", "date", "action", "referent", "file", "comment")


class BridgeTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-primary btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-bridge-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:bridge_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:bridge_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-bridge-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Bridge
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "visit_date",
            "interest",
            "renovated",
            "renovated_date",
            "joint",
            "rift",
            "expansion",
            "drain",
            "cornice",
            "volume",
            "comment",
        )


class TreeTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-primary btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-tree-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:tree_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:tree_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-tree-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    NBTAX = """
            {% if not record.bat_specie.all.count %}
            -
            {% else %}
            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                    data-target=".bs-tree-sp-modal-md-{{ record.pk }}">
            {{ record.bat_specie.all.count }}
            </button>
            <div class="modal fade bs-tree-sp-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="comment">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">Taxons observés</h4>
                        </div>
                        <div class="modal-body">
                        <table class="table table-striped">
                        <tr>

                            <th>nom français</th>
                            <th>nom scientifique</th>
                            <th>codesp</th>
                            <th>effectif</th>
                        </tr>
                        {% for d in record.bat_specie.all %}
                            <tr{% if d.sp_true %} class="success"{% endif %}>
                                <td>{{ d.common_name_fr }}</td>
                                <td>{{ d.sci_name }}</td>
                                <td>{{ d.codesp }}</td>
                                <td>{{ d.total_count }}</td>
                            </tr>
                        {% endfor %}
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            {% endif %}
        """
    GITEORIGIN = """
            {% if not record.gite_origin.all.count %}
            -
            {% else %}
            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                    data-target=".bs-gitorigin-modal-md-{{ record.pk }}">
            {{ record.gite_origin.all.count }}
            </button>
            <div class="modal fade bs-gitorigin-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="comment">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">Origine des gîtes</h4>
                        </div>
                        <div class="modal-body">
                        <ul>
                        {% for d in record.gite_origin.all %}
                            <li>{{ d.descr }}</li>
                        {% endfor %}
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            {% endif %}
        """
    GITETYPE = """
            {% if not record.gite_type.all.count %}
            -
            {% else %}
            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                    data-target=".bs-gittype-modal-md-{{ record.pk }}">
            {{ record.gite_type.all.count }}
            </button>
            <div class="modal fade bs-gittype-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="comment">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">Types de gîtes</h4>
                        </div>
                        <div class="modal-body">
                        <ul>
                        {% for d in record.gite_type.all %}
                            <li>{{ d.descr }}</li>
                        {% endfor %}
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            {% endif %}
        """
    GITELOC = """
            {% if not record.gite_localisation.all.count %}
            -
            {% else %}
            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                    data-target=".bs-gitloc-modal-md-{{ record.pk }}">
            {{ record.gite_localisation.all.count }}
            </button>
            <div class="modal fade bs-gitloc-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="comment">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">Localisation des gîtes</h4>
                        </div>
                        <div class="modal-body">
                        <ul>
                        {% for d in record.gite_localisation.all %}
                            <li>{{ d.descr }}</li>
                        {% endfor %}
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            {% endif %}
        """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    nbtax = tables.TemplateColumn(NBTAX, verbose_name="Nb de taxons")
    gitetype = tables.TemplateColumn(GITETYPE, verbose_name="type gites")
    giteorigin = tables.TemplateColumn(GITEORIGIN, verbose_name="origine gites")
    giteloc = tables.TemplateColumn(GITELOC, verbose_name="Loc. gites")
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Tree
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "visit_date",
            "context",
            "forest_stands",
            "situation",
            "circumstance",
            "tree_specie",
            "health",
            "tree_diameter",
            "standing",
            "protected",
            "nbtax",
            "gitetype",
            "giteorigin",
            "giteloc",
            "comment",
        )


class TreeGiteTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-primary btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-treegite-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:treegite_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:treegite_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-treegite-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    NBTAX = """
            {% if not record.bat_specie.all.count %}
            -
            {% else %}
            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                    data-target=".bs-treegite-sp-modal-md-{{ record.pk }}">
            {{ record.bat_specie.all.count }}
            </button>
            <div class="modal fade bs-treegite-sp-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="comment">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">Taxons observés</h4>
                        </div>
                        <div class="modal-body">
                        <table class="table table-striped">
                        <tr>

                            <th>nom français</th>
                            <th>nom scientifique</th>
                            <th>codesp</th>
                            <th>effectif</th>
                        </tr>
                        {% for d in record.bat_specie_set.all %}
                            <tr{% if d.codesp.sp_true %} class="success"{% endif %}>
                                <td>{{ d.codesp.common_name_fr }}</td>
                                <td>{{ d.codesp.sci_name }}</td>
                                <td>{{ d.codesp.codesp }}</td>
                                <td>{{ d.total_count }}</td>
                            </tr>
                        {% endfor %}
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            {% endif %}
        """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    nbtax = tables.TemplateColumn(NBTAX, verbose_name="Nb de taxons")
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = TreeGite
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "nbtax",
            "gite_type",
            "gite_origin",
            "gite_localisation",
            "gite_high",
            "gite_tree_diameter",
            "gite_access_orientation",
            "gite_access_size",
            "comment",
        )


class BuildTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-primary btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-build-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:build_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:build_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-build-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = Build
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "visit_date",
            "cavity_front",
            "attic",
            "attic_access",
            "bell_tower",
            "bell_tower_screen",
            "bell_tower_access",
            "cover",
            "ext_light",
            "access_light",
            "cellar",
            "cellar_access",
            "comment",
        )


class CaveTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-primary btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-cave-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:cave_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:cave_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-cave-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    WALK_DURATION = """
            {{ record.access_walk_duration|time:"H:i" }}"""
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)
    access_walk_duration = tables.TemplateColumn(
        WALK_DURATION, verbose_name="Durée de marche", orderable=False
    )

    class Meta:
        model = Cave
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "visit_date",
            "interest",
            "length",
            "altdiff",
            "n_entry",
            "equipment",
            "access_walk_duration",
            "comment",
        )


class PlaceSessionTable(tables.Table):
    ACTIONS = """
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-session-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:session_detail' record.pk %}" title="Fiche détaillée"><i class="fa fa-fw fa-info-circle"></i> Fiche
                    détaillée</a></li>
                <li><a href="{% url 'sights:session_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li><a href="{% url 'sights:sighting_edit_form' record.pk %}" Title="Editer les observations"><i
                        class="fi-eye"></i> Editer les observations</a></li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:session_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-session-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    NBTAX = """
            {% if not record.observations.all.count %}
            -
            {% else %}
            <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                    data-target=".bs-session-sp-modal-md-{{ record.pk }}">
            {{ record.observations.count }}
            </button>
            <div class="modal fade bs-session-sp-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="comment">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">Taxons observés</h4>
                        </div>
                        <div class="modal-body">
                        <table class="table table-striped">
                        <tr>

                            <th>nom français</th>
                            <th>nom scientifique</th>
                            <th>codesp</th>
                            <th>effectif</th>
                        </tr>
                        {% for d in record.observations.all %}
                            <tr{% if d.codesp.sp_true %} class="success"{% endif %}>
                                <td>{{ d.codesp.common_name_fr }}</td>
                                <td>{{ d.codesp.sci_name }}</td>
                                <td>{{ d.codesp.codesp }}</td>
                                <td>{{ d.total_count }}</td>
                            </tr>
                        {% endfor %}
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            {% endif %}
        """
    OBSERVERS = """
            {% if record.other_observer.all.count > 0 %}
                <a data-toggle="modal"
                   data-target=".bs-session-otherobs-modal-md-{{ record.pk }}"> {{ record.main_observer }} <span
                        class="badge">{{ record.other_observer.all.count }}</span></a>
                <div class="modal fade bs-session-otherobs-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                     aria-labelledby="otherobs">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="exampleModalLabel">Autres observateurs</h4>
                            </div>
                            <div class="modal-body">
                                {% for o in record.other_observer.all %}
                                    <b>{{ o.full_name }}</b> ({{ o.username }})<br/>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                </div>
            {% else %}
                {{ record.main_observer }}
            {% endif %}
            """
    # id = tables.Column(accessor='id_session', verbose_name='id')
    name = tables.LinkColumn(
        "sights:session_detail",
        args=[A("id_session")],
        accessor="name",
        verbose_name="Nom",
    )
    nbtax = tables.TemplateColumn(NBTAX, verbose_name="Nb de taxons", orderable=False)
    observer = tables.TemplateColumn(OBSERVERS, verbose_name="Observateur(s)")
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)
    date_start = tables.Column(verbose_name="Date début")
    study = tables.Column(verbose_name="Projet")
    date_end = tables.Column(verbose_name="Date fin")
    time_start = tables.Column(verbose_name="Heure début")
    time_end = tables.Column(verbose_name="Heure fin")
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)

    class Meta:
        model = Session
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "name",
            "contact.descr",
            "study",
            "date_start",
            "time_start",
            "date_end",
            "time_end",
            "observer",
            "nbtax",
            "comment",
        )


class SessionTable(PlaceSessionTable):
    place = tables.LinkColumn(
        "sights:place_detail", args=[A("place.id_place")], accessor="place.name"
    )

    class Meta:
        model = Session
        fields = PlaceSessionTable.Meta.fields + ("place",)


class SessionSightingTable(tables.Table):
    ACTIONS = """
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-sighting-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:sighting_detail' record.pk %}" title="Fiche détaillée"><i class="fa fa-fw fa-info-circle"></i> Fiche
                    détaillée</a></li>
                <li><a href="{% url 'sights:sighting_edit_form' record.session.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
            </ul>
        </div>
        <div class="modal fade bs-sighting-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    # id = tables.Column(accessor='id_sighting', verbose_name='id')
    common_name_fr = tables.LinkColumn(
        "sights:sighting_detail",
        args=[A("id_sighting")],
        accessor="codesp.common_name_fr",
        verbose_name="Taxon",
    )
    # common_name_fr = tables.Column(
    #     accessor='codesp.common_name_fr', verbose_name='Taxon')
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)
    sp_true = tables.BooleanColumn(accessor="codesp.sp_true", verbose_name=_("Espèce vraie"))
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)

    class Meta:
        model = Sighting
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "actions",
            "common_name_fr",
            "sp_true",
            "total_count",
            "breed_colo",
            "created_by",
            "comment",
        )


class SessionDeviceTable(tables.Table):
    ACTIONS = """
        <div class="btn-group btn-group-xs">
            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                Action <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a data-toggle="modal"
                       data-target=".bs-device-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                        class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                <li><a href="{% url 'sights:device_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                </li>
                <li role="separator" class="divider"></li>
                <li style="color:red;"><a href="{% url 'sights:device_delete' record.pk %}" title="Supprimer"><i
                        class="fa fa-fw fa-trash"></i> Supprimer</a></li>
            </ul>
        </div>
        <div class="modal fade bs-device-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
             aria-labelledby="history">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                    </div>
                    <div class="modal-body">
                        <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                            {% if record.timestamp_update %}
                                et mise à jour le {{ record.timestamp_update }}
                            {% endif %}
                            par {{ record.updated_by }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        """
    PHOTO_FILE = """
         {% if record.photo_file %}
         <button type="button" class="btn btn-primary btn-xs" data-toggle="modal"
                 data-target=".bs-device-comment-modal-md-{{ record.pk }}">
             <i class="fa fa-fw fa-camera" aria-hidden="true"></i>
         </button>
         <div class="modal fade bs-device-comment-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
              aria-labelledby="comment">
             <div class="modal-dialog modal-lg" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                         </button>
                         <h4 class="modal-title" id="exampleModalLabel">Photo du dispositif</h4>
                     </div>
                     <div class="modal-body">
                        <img src="{{ record.photo_file.url }}" width="100%"></img>
                     </div>
                 </div>
             </div>
         </div>
         {% endif %}
     """
    # id = tables.Column(accessor='id_device', verbose_name='id')
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)
    photo = tables.TemplateColumn(PHOTO_FILE, verbose_name="Photo", orderable=False)
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)

    class Meta:
        model = Device
        template = "table_bootstrap.html"
        attrs = table_class
        fields = ("actions", "ref", "type", "height", "width", "photo", "comment")


class SightingTable(tables.Table):
    ACTIONS = """
                <div class="btn-group btn-group-xs">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a data-toggle="modal"
                               data-target=".bs-sighting-history-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                                class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                        <li><a href="{% url 'sights:sighting_detail' record.pk %}" title="Fiche détaillée"><i class="fa fa-fw fa-info-circle"></i> Fiche
                            détaillée</a></li>
                        <li><a href="{% url 'sights:sighting_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                        </li>
                        <li><a href="{% url 'sights:countdetail_create' record.pk %}" Title="Ajouter une observation détaillée"><i
                                class="far fa-calendar-alt"></i> Ajouter une observation détaillée</a></li>
                        <li role="separator" class="divider"></li>
                        <li style="color:red;"><a href="{% url 'sights:sighting_delete' record.pk %}" title="Supprimer"><i
                                class="fa fa-fw fa-trash"></i> Supprimer</a></li>
                    </ul>
                </div>
                <div class="modal fade bs-sighting-history-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                     aria-labelledby="history">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                            </div>
                            <div class="modal-body">
                                <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                    {% if record.timestamp_update %}
                                        et mise à jour le {{ record.timestamp_update }}
                                    {% endif %}
                                    par {{ record.updated_by }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                """
    PLACETYPE = """
        {% if record.session.place.type %}
        <a data-toggle="popover" title="Description" data-content="{{ record.session.place.type.descr }}">{{ record.session.place.type.code }}</a>
        {% endif %}
    """
    METHOD = """
        {% if record.session.contact %}
        <a data-toggle="SightingSessionMethodToggle{{ record.pk }}">{{ record.session.contact.code }}</a>
            <div class="dropdown-pane" id="SightingSessionMethodToggle{{ record.pk }}" data-dropdown>
                {{ record.record.session.contact.descr }}
            </div>
        {% endif %}
    """
    id = tables.Column(
        accessor="id_sighting",
        attrs={"th": {"style": "display: none;"}, "td": {"style": "display:none;"}},
    )
    common_name_fr = tables.LinkColumn(
        "sights:sighting_detail",
        args=[A("id_sighting")],
        accessor="codesp.common_name_fr",
        verbose_name="Espèce",
    )
    sci_name = tables.Column(
        accessor="codesp.sci_name",
        verbose_name="Nom scientifique",
        attrs={"th": {"style": "display: none;"}, "td": {"style": "display:none;"}},
    )
    sp_true = tables.BooleanColumn(accessor="codesp.sp_true", verbose_name=_("Espèce vraie"))
    comment = tables.TemplateColumn(
        COMMENT, verbose_name="Commentaire", orderable=False, exclude_from_export=True
    )
    place = tables.LinkColumn(
        "sights:place_detail",
        args=[A("session.place.id_place")],
        accessor="session.place.name",
    )
    actions = tables.TemplateColumn(
        ACTIONS, verbose_name="Actions", orderable=False, exclude_from_export=True
    )
    placetype = tables.TemplateColumn(
        PLACETYPE,
        accessor="session.place.type.descr",
        verbose_name="Type de localité",
        exclude_from_export=True,
    )
    session = tables.LinkColumn(
        "sights:session_detail", args=[A("session.id_session")], accessor="session.name"
    )
    study = tables.Column(
        accessor="session.study", verbose_name=_("Etude"), exclude_from_export=True
    )
    export_x = tables.Column(
        accessor="session.place.geom.x",
        verbose_name="X",
        attrs={"th": {"style": "display: none;"}, "td": {"style": "display:none;"}},
    )
    export_y = tables.Column(
        accessor="session.place.geom.y",
        verbose_name="Y",
        attrs={"th": {"style": "display: none;"}, "td": {"style": "display:none;"}},
    )
    export_comment = tables.Column(
        accessor="comment",
        verbose_name=_("commentaire"),
        attrs={"th": {"style": "display: none;"}, "td": {"style": "display:none;"}},
    )
    export_placetype = tables.Column(
        accessor="session.place.type.descr",
        verbose_name=_("type_localite"),
        attrs={"th": {"style": "display: none;"}, "td": {"style": "display:none;"}},
    )

    class Meta:
        # export_name = 'sightings'
        model = Sighting
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "id",
            "actions",
            "common_name_fr",
            "sci_name",
            "sp_true",
            "total_count",
            "breed_colo",
            "session",
            "study",
            "session.date_start",
            "period",
            "session.contact.descr",
            "place",
            "placetype",
            "export_placetype",
            "export_x",
            "export_y",
            "observer.name_pretty",
            "comment",
            "export_comment",
        )


class SightingExportTable(tables.Table):
    id = tables.Column(accessor="id_sighting", verbose_name="id")
    common_name_fr = tables.Column(accessor="codesp.common_name_fr", verbose_name="Espèce")
    sp_true = tables.Column(accessor="codesp.sp_true", verbose_name="Espèce vraie")
    x = tables.Column(accessor="session.place.geom.x", verbose_name="X")
    y = tables.Column(accessor="session.place.geom.y", verbose_name="Y")
    place = tables.Column(accessor="session.place.name", verbose_name="Localité")
    placetype = tables.Column(accessor="session.place.type.descr", verbose_name="Type de localité")

    class Meta:
        export_name = "sightings.ext"
        model = Sighting
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "common_name_fr",
            "sp_true",
            "total_count",
            "breed_colo",
            "session",
            "place",
            "placetype",
            "created_by",
            "comment",
            "geom",
            "x",
            "y",
        )


class CountDetailBiomTable(tables.Table):
    TIME = "{{ record.time|time:'H:i' }}"
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)
    time = tables.TemplateColumn(TIME, verbose_name=_("Heure"), orderable=False)

    class Meta:
        model = CountDetail
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "method.descr",
            "sex",
            "age",
            "time",
            "device",
            "manipulator",
            "validator",
            "transmitter",
            "ab",
            "d5",
            "d3",
            "pouce",
            "queue",
            "tibia",
            "pied",
            "cm3",
            "tragus",
            "poids",
            "testicule.short_descr",
            "epididyme.short_descr",
            "tuniq_vag.short_descr",
            "gland_taille.short_descr",
            "gland_coul.short_descr",
            "mamelle.short_descr",
            "gestation.short_descr",
            "epiphyse.short_descr",
            "chinspot.short_descr",
            "usure_dent.short_descr",
            "comment",
        )


class CountDetailTelemetryTable(tables.Table):
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = CountDetail
        template = "table_bootstrap.html"
        attrs = table_class
        fields = ("time", "method", "transmitter", "comment")


class CountDetailOtherTable(tables.Table):
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = CountDetail
        template = "table_bootstrap.html"
        attrs = table_class
        fields = (
            "time",
            "method",
            "sex",
            "age",
            "count",
            "unit",
            "precision",
            "transmitter",
            "comment",
        )


class CountDetailAcousticTable(tables.Table):
    ACTIONS = """
            <div class="btn-group btn-group-xs">
                <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Action <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a data-toggle="modal"
                           data-target=".bs-cdacous-modal-md-{{ record.pk }}" title="Historique de la donnée"><i
                            class="fa fa-fw fa-history"></i> Historique de la donnée</a></li>
                                      {%comment%}<li><a href="{% url 'sights:countdetail_update' record.pk %}" title="Modifier"><i class="fa fa-fw fa-edit"></i> Modifier</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li style="color:red;"><a href="{% url 'sights:countdetail_delete' record.pk %}" title="Supprimer"><i
                            class="fa fa-fw fa-trash"></i> Supprimer</a></li>{%endcomment%}
                </ul>
            </div>
            <div class="modal fade bs-cdacous-modal-md-{{ record.pk }}" tabindex="-1" role="dialog"
                 aria-labelledby="history">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-fw fa-history"></i> Historique</h4>
                        </div>
                        <div class="modal-body">
                            <p>Donnée créée le {{ record.timestamp_create }} par {{ record.created_by }}
                                {% if record.timestamp_update %}
                                    et mise à jour le {{ record.timestamp_update }}
                                {% endif %}
                                par {{ record.updated_by }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            """
    actions = tables.TemplateColumn(ACTIONS, verbose_name="Actions", orderable=False)
    comment = tables.TemplateColumn(COMMENT, verbose_name="Commentaire", orderable=False)

    class Meta:
        model = CountDetail
        template = "table_bootstrap.html"
        attrs = table_class
        fields = ("actions", "time", "method", "count", "unit", "precision", "comment")
