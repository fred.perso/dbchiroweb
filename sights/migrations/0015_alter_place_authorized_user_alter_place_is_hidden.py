# Generated by Django 4.2.11 on 2024-10-15 21:35

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("sights", "0014_alter_bridge_extra_data_alter_build_extra_data_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="place",
            name="authorized_user",
            field=models.ManyToManyField(
                blank=True,
                related_name="hiddenplaceauthuser",
                to=settings.AUTH_USER_MODEL,
                verbose_name="Personnes authorisées si site confidentiel",
            ),
        ),
        migrations.AlterField(
            model_name="place",
            name="is_hidden",
            field=models.BooleanField(
                db_index=True, default=False, verbose_name="Site confidentiel"
            ),
        ),
    ]
