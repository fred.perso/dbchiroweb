from django.urls import path

from sights.session.views import (
    DeviceCreate,
    DeviceDelete,
    DeviceUpdate,
    SessionChangePlaceUpdate,
    SessionCreate,
    SessionDelete,
    SessionDetail,
    SessionLinkCreate,
    SessionLinkDelete,
    SessionLinkUpdate,
    SessionSearch,
    SessionUpdate,
)

from .api import SessionApi

urlpatterns = [
    # Session relative URLS
    path(
        "place/<int:pk>/session/add",
        SessionCreate.as_view(),
        name="session_create",
    ),
    path(
        "session/<int:pk>/detail",
        SessionDetail.as_view(),
        name="session_detail",
    ),
    path(
        "session/<int:pk>/update",
        SessionUpdate.as_view(),
        name="session_update",
    ),
    path(
        "session/<int:pk>/change_place",
        SessionChangePlaceUpdate.as_view(),
        name="sessionchangeplace_update",
    ),
    path(
        "session/<int:pk>/delete",
        SessionDelete.as_view(),
        name="session_delete",
    ),
    # Device relative URLS
    path(
        "session/<int:pk>/device/add",
        DeviceCreate.as_view(),
        name="device_create",
    ),
    path("device/<int:pk>/update", DeviceUpdate.as_view(), name="device_update"),
    path("device/<int:pk>/delete", DeviceDelete.as_view(), name="device_delete"),
    path(
        "session/<int:pk>/link/add",
        SessionLinkCreate.as_view(),
        name="sessionlink_create",
    ),
    path("sessionlink/<int:pk>/update", SessionLinkUpdate.as_view(), name="sessionlink_update"),
    path("sessionlink/<int:pk>/delete", SessionLinkDelete.as_view(), name="sessionlink_delete"),
    path("api/v1/sessions", SessionApi.as_view(), name="session_api"),
    path("session/search", SessionSearch.as_view(), name="session_search"),
]
