# from rest_framework.serializers import SerializerMethodField
import logging

from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework import serializers
from rest_framework_gis import serializers as gis_serializers

from accounts.serializer import ProfileSearchSerializer
from dicts.models import Contact, Specie
from geodata.models import Areas
from sights.countdetail.serializers import CountDetailBaseSerializer
from sights.models import CountDetail, Session, Sighting
from sights.place.serializers import SimplePlaceSerializer

logger = logging.getLogger(__name__)


class ObsSightingSpecieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Specie
        fields = (
            "codesp",
            "sci_name",
            "common_name_fr",
            "sp_true",
        )


class AreasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Areas
        fields = ("area_type", "code", "name")


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = (
            "descr",
            "code",
        )


class ObsSessionSerializer(serializers.ModelSerializer):
    place_data = SimplePlaceSerializer(source="place")
    contact = ContactSerializer()
    main_observer = ProfileSearchSerializer()

    class Meta:
        model = Session
        fields = (
            "id_session",
            "name",
            "contact",
            "date_start",
            "place_data",
            "main_observer",
        )


class SightingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sighting
        depth = 1
        fields = (
            "id_sighting",
            "codesp",
            "created_by",
            "session",
            "breed_colo",
            "total_count",
            "period",
            "is_doubtful",
            "comment",
            "timestamp_create",
            "created_by",
            "updated_by",
        )


class GeoSightingSerializer(gis_serializers.GeoFeatureModelSerializer):
    specie_data = ObsSightingSpecieSerializer(source="codesp")
    session_data = ObsSessionSerializer(source="session")
    creator = ProfileSearchSerializer(source="created_by")

    geom = gis_serializers.GeometrySerializerMethodField()
    # countdetail = SerializerMethodField()

    class Meta(SightingSerializer.Meta):
        depth = 0
        geo_field = "geom"
        fields = SightingSerializer.Meta.fields + (
            "specie_data",
            "creator",
            "session_data",
        )

    def get_geom(self, sighting):
        return sighting.session.place.geom


class EditSightingListSerializer(serializers.ListSerializer):
    def create(self, validated_data):
        sightings = [Sighting(**item) for item in validated_data]
        return sightings


class EditSightingSerializer(WritableNestedModelSerializer):
    countdetails = CountDetailBaseSerializer(many=True)
    extra_data = serializers.JSONField(allow_null=True)

    class Meta:
        model = Sighting
        fields = (
            "id_sighting",
            "session",
            "codesp",
            "breed_colo",
            "total_count",
            "observer",
            "is_doubtful",
            "comment",
            "extra_data",
            "created_by",
            "updated_by",
            "countdetails",
        )

    def get_countdetails(self, sighting):
        """Retrieve all child count details

        Args:
            sighting (Sighting): Sighting

        Returns:
            list: A list of child count details
        """
        countdetails = CountDetail.objects.filter(sighting=sighting.id_sighting).all()
        return countdetails
