# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet, ModelViewSet, mixins

from sights.mixins import LargeGeoJsonPageNumberPagination, SightingFilteredListWithPermissions
from sights.models import Sighting
from sights.observation.serializers import (  # EditSightingSerializer,
    EditSightingSerializer,
    GeoSightingSerializer,
)

from ..mixins import SightingListPermissionsMixin
from .permissions import SightingEditPermission

logger = logging.getLogger(__name__)
sighting_queryset = (
    Sighting.objects.select_related(
        "created_by",
        "updated_by",
        "observer",
        "session",
        "session__contact",
        "session__place",
        "session__main_observer",
        "session__study",
        "codesp",
        "session__place__type",
    )
    .prefetch_related("codesp__groupings")
    .prefetch_related("session__place__areas")
    .prefetch_related("session__place__areas__area_type")
    .prefetch_related("session__other_observer")
    .all()
)


class GeoJSONSighting(
    LoginRequiredMixin,
    SightingFilteredListWithPermissions,
    ListAPIView,
):
    queryset = sighting_queryset.distinct().order_by("-timestamp_update")
    serializer_class = GeoSightingSerializer
    pagination_class = LargeGeoJsonPageNumberPagination


class EditSighting(SightingListPermissionsMixin, ModelViewSet):
    """Edit observation viewset"""

    serializer_class = EditSightingSerializer
    permission_classes = [
        IsAuthenticated,
        SightingEditPermission,
    ]

    queryset = Sighting.objects.select_related("session").prefetch_related("countdetails").all()

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        if self.action in ("list", "create"):
            session = self.kwargs["session"]
            queryset = queryset.filter(session=session)
        else:
            queryset = queryset.all()
        return queryset

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class DeleteSighting(mixins.DestroyModelMixin, GenericViewSet):
    """Delete observation viewset"""

    serializer_class = EditSightingSerializer
    queryset = Sighting.objects.all()
    permission_classes = [
        IsAuthenticated,
        SightingEditPermission,
    ]
