from django.urls import path

from .export import CountDetailExport

urlpatterns = [
    path("countdetail/export", CountDetailExport.as_view(), name="countdetail_export"),
]
