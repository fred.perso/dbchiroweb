from django.urls import path

from metadata.api import AcquisitionFrameworkViewset, OrganismViewset
from metadata.views import (
    AcquisitionFrameworkCreate,
    AcquisitionFrameworkDetail,
    AcquisitionFrameworkUpdate,
    OrganismCreate,
    OrganismDelete,
    OrganismDetail,
    OrganismList,
    OrganismUpdate,
)

app_name = "metadata"

urlpatterns = [
    # API
    path(
        "api/v1/metadata/organisms/list",
        OrganismViewset.as_view({"get": "list"}),
        name="organism_list_api",
    ),
    path(
        "api/v1/metadata/organisms/<int:pk>",
        OrganismViewset.as_view({"get": "retrieve"}),
        name="organism_detail_api",
    ),
    path(
        "api/v1/metadata/acquisition_framework/list",
        AcquisitionFrameworkViewset.as_view({"get": "list"}),
        name="acquisition_framework_list_api",
    ),
    path(
        "api/v1/metadata/acquisition_framework/",
        AcquisitionFrameworkViewset.as_view({"post": "create"}),
        name="acquisition_framework_list_api",
    ),
    path(
        "api/v1/metadata/acquisition_framework/<int:pk>",
        AcquisitionFrameworkViewset.as_view({"get": "retrieve"}),
        name="acquisition_framework_detail_api",
    ),
    path(
        "api/v1/metadata/acquisition_framework/<int:pk>",
        AcquisitionFrameworkViewset.as_view({"put": "partial-update"}),
        name="acquisition_framework_list_api",
    ),
    path(
        "api/v1/metadata/acquisition_framework/<int:pk>",
        AcquisitionFrameworkViewset.as_view({"delete": "destroy"}),
        name="acquisition_framework_list_api",
    ),
    # Pages
    path(
        "metadata/organisms/list",
        OrganismList.as_view(),
        name="organisms_list",
    ),
    path(
        "metadata/organisms/create",
        OrganismCreate.as_view(),
        name="organisms_create",
    ),
    path(
        "metadata/organisms/<int:pk>/update",
        OrganismUpdate.as_view(),
        name="organisms_update",
    ),
    path(
        "metadata/organisms/<int:pk>/detail",
        OrganismDetail.as_view(),
        name="organisms_detail",
    ),
    path(
        "metadata/organisms/<int:pk>/delete",
        OrganismDelete.as_view(),
        name="organisms_delete",
    ),
    path(
        "metadata/acquisition-framework/create",
        AcquisitionFrameworkCreate.as_view(),
        name="acquisition_framework_create",
    ),
    path(
        "metadata/acquisition-framework/<int:pk>/update",
        AcquisitionFrameworkUpdate.as_view(),
        name="acquisition_framework_update",
    ),
    path(
        "metadata/acquisition-framework/<int:pk>/detail",
        AcquisitionFrameworkDetail.as_view(),
        name="acquisition_framework_detail",
    ),
]
