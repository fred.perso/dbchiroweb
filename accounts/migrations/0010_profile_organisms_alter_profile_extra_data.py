# Generated by Django 4.2.11 on 2024-09-27 20:39

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0009_alter_profile_organism_txt"),
    ]

    operations = [
        migrations.AlterField(
            model_name="profile",
            name="extra_data",
            field=models.JSONField(default=dict, verbose_name="Données additionnelles"),
        ),
    ]
