from django.urls import path

from .api import AreaListApi

app_name = "geodata"

urlpatterns = [
    # API
    path(
        "api/v1/geodata/areas/list",
        AreaListApi.as_view(),
        name="organism_list_api",
    ),
]
