from django.conf import settings
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from dicts.models import AreaType, LandCoverCLC

# =========================================================================
# Données géographiques
# =========================================================================


class Areas(models.Model):
    area_type = models.ForeignKey(
        AreaType, on_delete=models.CASCADE, db_index=True, null=False, blank=False
    )
    name = models.CharField(max_length=250, verbose_name="Nom", db_index=True)
    code = models.CharField(max_length=100, verbose_name="Code", db_index=True)
    coverage = models.BooleanField(default=False, verbose_name="Territoire couvert", db_index=True)
    geom = models.MultiPolygonField(
        srid=settings.GEODATA_SRID,
        verbose_name="Emprise géographique",
        spatial_index=True,
    )

    def __str__(self):
        return "%s ∙ %s" % (self.code, self.name)

    class Meta:
        verbose_name = _("Zonage géographique")
        verbose_name_plural = _("Zonages géographiques")


class LandCover(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.ForeignKey(
        LandCoverCLC,
        verbose_name=_("Code"),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    geom = models.MultiPolygonField(
        srid=settings.GEODATA_SRID,
        verbose_name=_("Géométries"),
        spatial_index=True,
    )

    def __str__(self):
        return "%s %s" % (self.code, self.code_descr.descr)

    # TODO Créer model dico CLC12

    class Meta:
        verbose_name = _("Occupation du sol")
        verbose_name_plural = _("Occupation du sol")


class Elevation(models.Model):
    name = models.CharField(max_length=100)
    rast = models.RasterField(srid=settings.GEODATA_SRID, spatial_index=True)

    def __str__(self):
        return f"#{self.pk} {self.name}"

    class Meta:
        verbose_name = _("Altitude")
