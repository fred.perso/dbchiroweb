SELECT *
FROM bdchironalpesra.datas
WHERE id_data NOT IN (
  SELECT DISTINCT od.id_data
  FROM bdchironalpesra.datas od
    LEFT JOIN sights_place sp ON od.id_loc = sp.id_bdsource
    LEFT JOIN bdchironalpesra.datas_observateurs oo ON od.observ_1 = oo.nom_complet
    LEFT JOIN accounts_profile ap ON (ap.first_name, ap.last_name) = (oo.prenom, oo.nom)
    LEFT JOIN dicts_contact dc ON lower(contact) = dc.code
    LEFT JOIN sights_session ss ON ss.name = concat('loc',
                                                    sp.id_place,
                                                    '_',
                                                    make_date(an, mois, jour), '_',
                                                    dc.code)
    LEFT JOIN dicts_specie ds ON replace(lower(od.codesp), ' ', '') LIKE ds.codesp
  WHERE make_date(an, mois, jour) IS NOT NULL AND sp.id_place IS NOT NULL
  --         AND ss.id_session = 2949
  GROUP BY
    make_date(an, mois, jour), od.id_data, dc.id, sp.id_place, ds.codesp, ss.id_session,
    ds.id);
