from django.db.models import Manager


class SpeciesManager(Manager):
    """Species custom manager"""

    def get_by_natural_key(self, codesp: str):
        """Get by natural key function for species

        Args:
            codesp (str): _description_

        Returns:
            _type_: _description_
        """
        return self.get(codesp=codesp)


class DictsBaseManager(Manager):
    """Dicts custom manager"""

    def get_by_natural_key(self, code: str):
        """Get by natural key function for organisms

        Args:
            code (str): _description_

        Returns:
            _type_: _description_
        """
        print(f"<get_by_natural_key> self {self} {dir(self)}")
        print(f"<get_by_natural_key> code {code}")
        return self.get(code=code)
